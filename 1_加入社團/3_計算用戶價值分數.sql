CREATE OR REPLACE TABLE analysis.user_value_score_browsing_user_1_join_club AS 

WITH tab_raw_behavior AS (
  SELECT *
  FROM temp_data.js_user_value_ip_behavior_with_detail
  WHERE behavior IS NOT NULL
)
, tab_cal_behavor_cnt AS (
  SELECT
    openid,
    ip_id,
    behavior,
    COUNT(*) AS cnt_behavior
  FROM tab_raw_behavior
  GROUP BY 1,2,3
)
, tab_get_perc_by_behavior_IP AS (
  SELECT openid, ip_id, flg, AVG(perc) AS perc
  FROM (
    SELECT 
      *, 
      DENSE_RANK() OVER (PARTITION BY ip_id, behavior ORDER BY cnt_behavior)/COUNT(DISTINCT cnt_behavior) OVER (PARTITION BY ip_id, behavior) AS perc,
      IF(behavior IN ("attend_show_for_award","campaign_cmt", "cmt_show_for_award"), "campaign", "exploration") AS flg
    FROM tab_cal_behavor_cnt
  )
  GROUP BY 1,2,3
)
, tab_cal_user_score AS (
  SELECT openid, ip_id, COALESCE(exploration, 0) AS exploration, COALESCE(campaign, 0) AS campaign
  FROM tab_get_perc_by_behavior_IP
  PIVOT (
    AVG(perc)
    FOR flg IN ("exploration", "campaign")
  )
  ORDER BY openid, ip_id
)

  SELECT
    t1.openid,
    t1.ip_id AS club_id,
    t2.ip_name AS club_name,
    t1.exploration,
    t1.campaign,
    CASE
      WHEN exploration = 0 THEN "優惠獵人"
      WHEN campaign = 0 AND exploration > 0.25 THEN "IP內容粉"
      WHEN campaign <=0.25  AND exploration <= 0.25 THEN "過客"
      WHEN campaign >0.25 AND exploration <=0.25 THEN "偶爾看內容的優惠獵人"
      WHEN campaign <=0.25  AND exploration > 0.25 THEN "在乎內容也好奇優惠"
      ELSE "IP狂熱我全都要"
    END AS type
  FROM tab_cal_user_score t1
  INNER JOIN `analysis.js_user_value_ip_list` t2
  USING (ip_id)

  