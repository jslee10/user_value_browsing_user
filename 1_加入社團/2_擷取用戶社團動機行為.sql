CREATE OR REPLACE TABLE temp_data.js_user_value_ip_behavior_with_detail AS 

WITH base_club_behavior AS (
  SELECT 
    date, t1.openid, t1.ip_id,ip_name,
    JSON_EXTRACT_ARRAY(visit_club) AS visit_club,
    JSON_EXTRACT_ARRAY(click_post) AS click_post,
    like_post AS like_post,
    JSON_EXTRACT_ARRAY(cmt_post) AS cmt_post,
    attend_show
  FROM temp_data.js_user_value_club_behavior_with_process t1
)
, base_club_swap AS (
  SELECT *
  FROM `temp_data.js_user_value_deep_use_by_session`
)
, tab_swap_in_club AS (
  SELECT
    date, openid, ip_id, ip_name,
    "swap_in_club" AS behavior,
    PARSE_JSON(swap_club) AS detail
  FROM base_club_swap
  WHERE ARRAY_LENGTH(JSON_EXTRACT_ARRAY(JSON_EXTRACT(swap_club, "$.post_id")))>3
) 
, tab_cmt_opinion AS (
  SELECT t1.* EXCEPT(post_id, cmt_arr), TO_JSON(STRUCT(post_id AS post_id, cmt AS cmt)) AS detail
  FROM (
    SELECT 
      date,
      openid,
      ip_id,
      ip_name,
      IF(REGEXP_CONTAINS(t2.content, "指定任務|留言|獲得|抽獎|加票|得獎|中獎"), "campaign_cmt", "opinion_cmt") AS behavior,
      JSON_EXTRACT_SCALAR(cmt_post, "$.post_id") AS post_id,
      JSON_EXTRACT_STRING_ARRAY(JSON_EXTRACT(cmt_post, "$.cmt")) AS cmt_arr
    FROM base_club_behavior t1, UNNEST(cmt_post) AS cmt_post
    INNER JOIN `server_data.beanfun_posts` t2
    ON JSON_EXTRACT_SCALAR(cmt_post, "$.post_id")  = t2.id
  ) t1, UNNEST(cmt_arr) AS cmt
)
, tab_emoji_feeling AS (
  SELECT
    date, openid, ip_id, ip_name,
    "emoji_feeling" AS behavior,
    PARSE_JSON(like_post) AS detail
  FROM base_club_behavior
  WHERE ARRAY_LENGTH(JSON_EXTRACT_ARRAY(like_post))>0
)
, campaign_show AS (
  SELECT DISTINCT content_id AS show_id, club_id
  FROM `bf-data-normal-001.content.beanfun_show`
  WHERE topic_id IN ("31", "56", "194")
)
, campaign_showItem AS (
  SELECT DISTINCT t1.content_id AS showItem_id, t2.*
  FROM `content.beanfun_show_item` t1
  INNER JOIN campaign_show t2
  USING (show_id)
)
, tab_attend_show_for_award AS (
  SELECT
    DATE(TIMESTAMP_MILLIS(CAST(publish_time AS INT64)), "Asia/Taipei") AS date,
    t1.owner_open_id AS openid,
    t2.club_id AS ip_id,
    t3.ip_name,
    "attend_show_for_award" AS behavior,
    TO_JSON(STRUCT(show_id AS show_id, content_id AS showItem_id)) AS detail
  FROM `content.beanfun_show_item` t1
  INNER JOIN campaign_show t2
  USING (show_id)
  INNER JOIN `analysis.js_user_value_ip_list` t3
  ON t2.club_id = t3.ip_id
)
, tab_cmt_show_for_award AS (
  SELECT 
    DATE(TIMESTAMP_MILLIS(CAST(t1.created_time AS INT64)), "Asia/Taipei") AS date,
    t2.open_id AS openid,
    t3.club_id AS ip_id,
    t4.ip_name,
    "cmt_show_for_award" AS behavior,
    TO_JSON(STRUCT(show_id AS show_id, t1.content AS cmt)) AS detail
  FROM `server_data.beanfun_comment` t1
  INNER JOIN `server_data.beanfun_aliasid_openid_mapping` t2
  ON t1.alias_id = t2.alias_id
  INNER JOIN campaign_show t3
  ON t1.entity_id = t3.show_id
  INNER JOIN `analysis.js_user_value_ip_list` t4
  ON t3.club_id = t4.ip_id
  WHERE entity_type = "show"
  UNION ALL
  SELECT 
    DATE(TIMESTAMP_MILLIS(CAST(t1.created_time AS INT64)), "Asia/Taipei") AS date,
    t2.open_id AS openid,
    t3.club_id AS ip_id,
    t4.ip_name,
    "cmt_show_for_award" AS behavior,
    TO_JSON(STRUCT(show_id AS show_id, t1.content AS cmt)) AS detail
  FROM `server_data.beanfun_comment` t1
  INNER JOIN `server_data.beanfun_aliasid_openid_mapping` t2
  ON t1.alias_id = t2.alias_id
  INNER JOIN campaign_showItem t3
  ON t1.entity_id = t3.showItem_id
  INNER JOIN `analysis.js_user_value_ip_list` t4
  ON t3.club_id = t4.ip_id
  WHERE entity_type = "showItem"
)
, tab_base_list AS (
  SELECT *
  FROM analysis.base_list_oid_join_ip_club_date
)
, var_all AS (
  SELECT *
  FROM tab_swap_in_club
  UNION ALL
  SELECT *
  FROM tab_cmt_opinion
  UNION ALL
  SELECT *
  FROM tab_emoji_feeling
  UNION ALL
  SELECT *
  FROM tab_attend_show_for_award
  UNION ALL
  SELECT *
  FROM tab_cmt_show_for_award
)

  SELECT
    CASE 
      WHEN t1.date > t2.date  THEN "after_join"
      WHEN t1.date = t2.date THEN "join"
      WHEN t1.date < t2.date THEN "before_join"
      ELSE "join"
    END AS process,
    COALESCE(t2.date, t1.date) AS date,
    COALESCE(t2.openid, t1.openid) AS openid,
    COALESCE(t2.ip_id, t1.club_id) AS ip_id,
    COALESCE(t2.ip_id, t1.ip_name) AS ip_name,
    t2.behavior,
    t2.detail
  FROM tab_base_list t1
  LEFT JOIN var_all t2
  ON t1.openid = t2.openid AND t1.club_id = t2.ip_id