CREATE OR REPLACE TABLE temp_data.js_user_value_club_behavior_with_process AS 

WITH base AS (
  SELECT
    DATE(TIMESTAMP_MILLIS(CAST(timestamp AS INT64)), "Asia/Taipei") AS date,
    openid,
    CAST(timestamp AS FLOAT64) AS timestamp,
    CAST(event_index AS FLOAT64) AS event_index,
    event,
    page_info,
    click_info
  FROM `event.event_daily`
  WHERE property = "beanfun"
  AND event_trigger != "impression"
  AND date BETWEEN 20220101 AND 20240622
  AND openid IS NOT NULL
)

, tab_visit_club AS (
  SELECT
    date,
    openid,
    JSON_EXTRACT_SCALAR(page_info, "$.uuid") AS club_id,
    JSON_EXTRACT_SCALAR(page_info, "$.source") AS source
  FROM base
  WHERE event = "club_page_view"
  AND JSON_EXTRACT_SCALAR(page_info, "$.page") = "club_home"
  AND JSON_EXTRACT_SCALAR(page_info, "$.uuid") IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
  )
)
, tab_click_post AS (
  SELECT DISTINCT
    date,
    t1.openid,
    JSON_EXTRACT_SCALAR(page_info, "$.uuid") AS post_id,
    t3.club_id,
    JSON_EXTRACT_SCALAR(page_info, "$.source") AS source,
  FROM base t1
  INNER JOIN `server_data.beanfun_posts` t3
  ON JSON_EXTRACT_SCALAR(page_info, "$.uuid") = t3.id
  WHERE club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
    
  )
  AND event = "club_post_page_view"
)
, tab_like_post AS (
  SELECT DISTINCT
    PARSE_DATE("%Y%m%d", CAST(date AS STRING)) AS date,
    t2.open_id AS openid,
    t1.post_id,
    t3.club_id
  FROM `server_data.beanfun_post_reactions` t1
  INNER JOIN `server_data.beanfun_aliasid_openid_mapping` t2
  ON t1.user_id = t2.alias_id
  INNER JOIN `server_data.beanfun_posts` t3
  ON t1.post_id = t3.id
  WHERE t3.club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
      
  )
)
, tab_cmt_post AS (
  SELECT
    PARSE_DATE("%Y%m%d", CAST(date AS STRING)) AS date,
    t2.open_id AS openid,
    entity_id AS post_id,
    t3.club_id,
    ARRAY_AGG(t1.content) AS content
  FROM `server_data.beanfun_comment` t1
  INNER JOIN `server_data.beanfun_aliasid_openid_mapping` t2
  ON t1.alias_id = t2.alias_id
  INNER JOIN `server_data.beanfun_posts` t3
  ON t1.entity_id = t3.id
  WHERE entity_type = "clubPost"
  AND t3.club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
      
  )
  GROUP BY 1,2,3,4
)
, tab_attend_show AS (
  SELECT DISTINCT
    DATE(TIMESTAMP_MILLIS(CAST(t1.publish_time AS INT64)), "Asia/Taipei") AS date,
    owner_open_id AS openid,
    t2.club_id
  FROM `bf-data-normal-001.content.beanfun_show_item` t1
  INNER JOIN `content.beanfun_show` t2
  ON t1.show_id = t2.content_id
  WHERE club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
      
  )
)
, tab_join_club AS (
  WITH tmp AS (
    SELECT 
      DATE(TIMESTAMP_MILLIS(CAST(created_time AS INT64)), "Asia/Taipei") AS date,
      user_id,
      club_id
    FROM `server_data.beanfun_club_participants`
    WHERE club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
      
    )
    AND role = "10"
    UNION DISTINCT
    SELECT 
      DATE(TIMESTAMP_MILLIS(CAST(join_time AS INT64)), "Asia/Taipei") AS date,
      user_id,
      club_id
    FROM `server_data.beanfun_club_participants_deleted`
    WHERE club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
      
    )
    AND role = "10"
  )
  SELECT
    t2.open_id AS openid,
    t1.club_id,
    MIN(t1.date) AS date
  FROM tmp t1
  INNER JOIN `server_data.beanfun_aliasid_openid_mapping` t2
  ON t1.user_id = t2.alias_id
  GROUP BY 1,2
)
, var_visit_club AS (
  WITH tmp AS (
    SELECT 
      date,
      openid,
      club_id AS ip_id,
      source,
      COUNT(*) AS cnt
    FROM tab_visit_club
    GROUP BY 1,2,3,4
  )
  , tmp_json AS (
    SELECT
      date, openid, ip_id,
      TO_JSON(STRUCT(source AS source, cnt AS cnt)) AS visit_club
    FROM tmp
  )
  SELECT
    date, openid, ip_id, TO_JSON_STRING(ARRAY_AGG(visit_club)) AS visit_club
  FROM tmp_json
  GROUP BY 1,2,3
)
, var_click_post AS (
  WITH tmp AS (
    SELECT 
      date,
      openid,
      club_id AS ip_id,
      post_id,
      source,
      COUNT(*) AS cnt
    FROM tab_click_post
    GROUP BY 1,2,3,4,5
  )
  , tmp_json AS (
    SELECT
      date, openid, ip_id,
      TO_JSON(STRUCT(post_id AS post_id, source AS source, cnt AS cnt)) AS click_post
    FROM tmp
  )
  SELECT
    date, openid, ip_id, TO_JSON_STRING(ARRAY_AGG(click_post)) AS click_post
  FROM tmp_json
  GROUP BY 1,2,3
)
, var_like_post AS (
  WITH tmp_json AS (
    SELECT
      date, openid, club_id AS ip_id,
      TO_JSON(STRUCT(post_id AS post_id)) AS like_post
    FROM tab_like_post
  )
  SELECT
    date, openid, ip_id, TO_JSON_STRING(ARRAY_AGG(like_post)) AS like_post
  FROM tmp_json
  GROUP BY 1,2,3
)
, var_cmt_post AS (
  WITH tmp_json AS (
    SELECT
      date, openid, club_id AS ip_id,
      TO_JSON(STRUCT(post_id AS post_id, content AS cmt)) AS cmt_post
    FROM tab_cmt_post
  )
  SELECT
    date, openid, ip_id, TO_JSON_STRING(ARRAY_AGG(cmt_post)) AS cmt_post
  FROM tmp_json
  GROUP BY 1,2,3
)
, var_attend_show AS (
  SELECT date, openid, club_id AS ip_id, 1 AS attend_show
  FROM tab_attend_show
)
, var_join_club AS (
  SELECT date, openid, club_id AS ip_id, 1 AS join_club
  FROM tab_join_club
)
, var_merge AS (
  SELECT *
  FROM (
  SELECT *
    FROM var_visit_club t1
    FULL JOIN var_click_post t2
    USING (date, openid, ip_id)
    FULL JOIN var_like_post t3
    USING (date, openid, ip_id)
    FULL JOIN var_cmt_post t4
    USING (date, openid, ip_id)
    FULL JOIN var_attend_show t5
    USING (date, openid, ip_id)
    FULL JOIN var_join_club t6
    USING (date, openid, ip_id)
  )
  WHERE date BETWEEN "2022-01-01" AND "2024-06-22"
  -- AND openid = "2174282596391964672"
  -- AND ip_id = "698993937515294720"
)


SELECT 
    CASE 
      WHEN t1.date > t2.date  THEN "after_join"
      WHEN t1.date = t2.date THEN "join"
      ELSE "before_join"
    END AS process,
  t1.date AS date,
  t1.openid AS openid,
  t1.ip_id AS ip_id,
  t3.ip_name,
  t1.* EXCEPT(date, openid, ip_id),
FROM var_merge t1
LEFT JOIN tab_join_club t2
ON t1.openid = t2.openid AND t1.ip_id = t2.club_id
INNER JOIN `analysis.js_user_value_ip_list` t3
ON t1.ip_id = t3.ip_id OR t2.club_id = t3.ip_id
ORDER BY openid, ip_id, date