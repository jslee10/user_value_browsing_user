CREATE OR REPLACE TABLE analysis.user_value_list_use_20220101_20240622 AS

WITH base_level_1 AS (
  SELECT *
  FROM `analysis.user_value_score_browsing_user_1_join_club`
)
, club_level_1 AS (
  SELECT DISTINCT 
    openid,
    club_id,
    ML.MIN_MAX_SCALER((exploration+campaign)/2) OVER () AS score,
    1 AS level
  FROM base_level_1
  WHERE type IN ("IP狂熱我全都要", "在乎內容也好奇優惠", "IP內容粉")
  OR (exploration >= 0.1 AND campaign <= 0.02)
)
, platform_level_1 AS (
  WITH score_max AS (
    SELECT
      openid,
      MAX((exploration+campaign)/2) AS score
    FROM base_level_1
    WHERE type IN ("IP狂熱我全都要", "在乎內容也好奇優惠", "IP內容粉")
    OR (exploration >= 0.1 AND campaign <= 0.02)
    GROUP BY openid
  )
  SELECT DISTINCT
    openid,
    ML.MIN_MAX_SCALER(score) OVER () AS score,
    1 AS level
  FROM score_max
)
, base_level_2 AS (
  SELECT *, AVG(active_score) OVER (PARTITION BY club_id) AS avg_active_score_by_club
  FROM `analysis.user_value_score_browsing_user_2_contact_post`
)
, club_level_2 AS (
  SELECT DISTINCT
    openid,
    club_id,
    ML.MIN_MAX_SCALER(week_score+week_score*active_score) OVER() AS score,
    2 AS level
  FROM base_level_2
  WHERE week_score >= 11 
  OR (week_score >= 6 AND active_score >= avg_active_score_by_club AND active_score > 0)
)
, platform_level_2 AS (
  SELECT
    openid,
    ML.MIN_MAX_SCALER(score) OVER() AS score, 
    2 AS level
  FROM (
    SELECT DISTINCT
      openid,
      MAX(week_score+week_score*active_score) AS score,
    FROM base_level_2
    WHERE week_score >= 11 
    OR (week_score >= 6 AND active_score >= avg_active_score_by_club AND active_score > 0)
    GROUP BY openid
  )
)
, base_level_3 AS (
  SELECT *
  FROM `analysis.user_value_score_browsing_user_3_deep_use`
)
, club_level_3 AS (
  SELECT
    COALESCE(t1.openid, t2.openid) AS openid,
    COALESCE(t1.club_id, t2.club_id) AS club_id,
    ROUND((ML.MIN_MAX_SCALER(COALESCE(score_breadth,0)) OVER() + ML.MIN_MAX_SCALER(COALESCE(score_depth,0)) OVER())/2, 2) AS score,
    3 AS level
  FROM (
    SELECT 
      openid,
      club_id,
      cnt_week*corr_score AS score_breadth
    FROM base_level_3
    WHERE cnt_week >= 6 
    OR (cnt_week >= 3 AND corr_score > 0.25)
  ) t1
  FULL JOIN (
    SELECT
      openid,
      club_id,
      cnt_post_cmt_gesture * ctr_cmt_gesture AS score_depth
    FROM base_level_3 
    WHERE cnt_post_cmt_gesture > 10
  ) t2
  USING (openid, club_id)
)
, platform_level_3 AS (
  SELECT
    COALESCE(t1.openid, t2.openid) AS openid,
    ROUND((ML.MIN_MAX_SCALER(COALESCE(score_breadth,0)) OVER() + ML.MIN_MAX_SCALER(COALESCE(score_depth,0)) OVER())/2, 2) AS score,
    3 AS level
  FROM (
    SELECT 
      openid,
      MAX(cnt_week*corr_score) AS score_breadth
    FROM base_level_3
    WHERE cnt_week >= 6 
    OR (cnt_week >= 3 AND corr_score > 0.25)
    GROUP BY openid
  ) t1
  FULL JOIN (
    SELECT
      openid,
      SUM(cnt_post_cmt_gesture) * AVG(ctr_cmt_gesture) AS score_depth
    FROM base_level_3 
    WHERE cnt_post_cmt_gesture > 28
    GROUP BY openid
  ) t2
  USING (openid)
)
, create_list_platform AS (
  SELECT DISTINCT
    COALESCE(t3.openid, t2.openid, t1.openid) AS openid,
    "" AS club_id,
    COALESCE(t3.level, t2.level, t1.level) AS value_level,
    COALESCE(t3.score, t2.score, t1.score) AS score,
    "club" AS club_or_platform,
    "使用" AS value_type
  FROM platform_level_1 t1
  LEFT JOIN platform_level_2 t2
  ON t1.openid = t2.openid
  LEFT JOIN platform_level_3 t3
  ON t1.openid = t3.openid AND t2.openid = t3.openid
)
, create_list_club AS (
  SELECT DISTINCT
    COALESCE(t3.openid, t2.openid, t1.openid) AS openid,
    COALESCE(t3.club_id, t2.club_id, t1.club_id) AS club_id,
    COALESCE(t3.level, t2.level, t1.level) AS value_level,
    COALESCE(t3.score, t2.score, t1.score) AS score,
    "club" AS club_or_platform,
    "使用" AS value_type
  FROM club_level_1 t1
  LEFT JOIN club_level_2 t2
  ON t1.openid = t2.openid AND t1.club_id = t2.club_id
  LEFT JOIN club_level_3 t3
  ON t1.openid = t3.openid AND t2.openid = t3.openid AND t1.club_id = t3.club_id AND t2.club_id = t3.club_id
)

  SELECT *
  FROM create_list_platform
  UNION DISTINCT
  SELECT *
  FROM create_list_club