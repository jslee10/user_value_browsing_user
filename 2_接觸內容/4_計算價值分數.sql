
CREATE OR REPLACE TABLE analysis.user_value_score_browsing_user_2_contact_post AS

WITH tmp AS (
  SELECT DISTINCT
    openid,
    cycle,
    channel,
    CASE 
      WHEN channel IN ("探索-我的社團列", "search", "頭像-我的社團列", "所有社團頁", "探索-無腦刷", "show", "profile", "exploration") THEN "active"
      WHEN channel IN ("notifications", "home", "deeplink", "chatroom", "splash", "acg", "anytix") THEN "passive"
      ELSE NULL
    END AS channel_type
  FROM temp_data.js_user_value_contact_post_channel
)
, tab_concat_path_by_cycle AS (
  SELECT
    t2.user_type,
    t1.openid,
    t1.cycle,
    t2.club_id,
    t1.channel,
    t1.channel_type
  FROM tmp t1
  INNER JOIN `temp_data.js_user_value_contact_post_contact_week` t2
  ON t1.openid = t2.openid AND t1.cycle = t2.cycle
  WHERE channel_type IS NOT NULL
)
, tab_user_contact_week AS (
  SELECT 
    user_type,
    openid,
    club_id,
    MAX(contact_order) AS cnt_contact
  FROM temp_data.js_user_value_contact_post_contact_week
  WHERE user_type != "優惠獵人"
  GROUP BY 1,2,3
)
, tab_channel_perc AS (
  SELECT DISTINCT
    user_type,
    openid,
    club_id,
    channel_type,
    COUNT(*) OVER (PARTITION BY user_type, openid, club_id, channel_type)/COUNT(*) OVER (PARTITION BY user_type, openid, club_id) AS channel_perc,
    COUNT(*) OVER (PARTITION BY user_type, openid, club_id) AS ttl_contact_by_oid_club
  FROM tab_concat_path_by_cycle t1
  ORDER BY openid, club_id, channel_type
)

  SELECT DISTINCT
    t1.user_type,
    t1.openid,
    t1.club_id,
    t3.ip_name AS club_name,
    COALESCE(channel_perc, 0) AS active_score,
    t1.cnt_contact AS week_score
  FROM tab_user_contact_week t1
  LEFT JOIN (
    SELECT *
    FROM tab_channel_perc
    WHERE channel_type = "active"
  ) t2
  USING (openid, club_id)
  INNER JOIN `analysis.js_user_value_ip_list` t3
  ON t1.club_id = t3.ip_id