CREATE OR REPLACE TABLE temp_data.js_user_value_contact_post_channel AS

WITH tab_first_club_event AS (
  SELECT DISTINCT
    DATE(datetime) AS date,
    openid,
    cycle,
    FIRST_VALUE(timestamp) OVER (PARTITION BY openid, cycle ORDER BY timestamp, CAST(event_index AS FLOAT64)) AS timestamp,
    FIRST_VALUE(event) OVER (PARTITION BY openid, cycle ORDER BY timestamp, CAST(event_index AS FLOAT64)) AS event,
    FIRST_VALUE(page_info) OVER (PARTITION BY openid, cycle ORDER BY timestamp, CAST(event_index AS FLOAT64)) AS page_info,
    FIRST_VALUE(lag_event) OVER (PARTITION BY openid, cycle ORDER BY timestamp, CAST(event_index AS FLOAT64)) AS lag_event,
    FIRST_VALUE(lag_page_info) OVER (PARTITION BY openid, cycle ORDER BY timestamp, CAST(event_index AS FLOAT64)) AS lag_page_info,
    FIRST_VALUE(lag_click_info) OVER (PARTITION BY openid, cycle ORDER BY timestamp, CAST(event_index AS FLOAT64)) AS lag_click_info
  FROM `temp_data.js_user_value_contact_post_raw_event_2`
  WHERE event_ts = timestamp
  AND event IN ("club_home_content_impression", "home_page_impression", "exploration_page_content_impression", "club_post_page_view")
)
, tab_club_source_event AS (
  WITH tab_event AS (
    SELECT DISTINCT date, openid, cycle, timestamp, event AS contact_event
    FROM tab_first_club_event
    WHERE 
    (
      event = "club_post_page_view"
      AND JSON_EXTRACT_SCALAR(page_info, "$.source") IN ("club_home", "club")
      AND lag_event LIKE "club%"
    )
    OR
    (
      event = "club_home_content_impression"
    )
  )
  SELECT DISTINCT
    DATE(datetime) AS date,
    t1.openid,
    t1.cycle,
    t2.contact_event,
    FIRST_VALUE(t1.timestamp) OVER (PARTITION BY t1.openid, t1.cycle ORDER BY t1.timestamp, CAST(event_index AS FLOAT64)) AS timestamp,
    FIRST_VALUE(event) OVER (PARTITION BY t1.openid, t1.cycle ORDER BY t1.timestamp, CAST(event_index AS FLOAT64)) AS event,
    FIRST_VALUE(page_info) OVER (PARTITION BY t1.openid, t1.cycle ORDER BY t1.timestamp, CAST(event_index AS FLOAT64)) AS page_info,
    FIRST_VALUE(lag_event) OVER (PARTITION BY t1.openid, t1.cycle ORDER BY t1.timestamp, CAST(event_index AS FLOAT64)) AS source_event,
    FIRST_VALUE(lag_page_info) OVER (PARTITION BY t1.openid, t1.cycle ORDER BY t1.timestamp, CAST(event_index AS FLOAT64)) AS source_page_info,
    FIRST_VALUE(lag_click_info) OVER (PARTITION BY t1.openid, t1.cycle ORDER BY t1.timestamp, CAST(event_index AS FLOAT64)) AS source_click_info
  FROM `temp_data.js_user_value_contact_post_raw_event_2` t1
  INNER JOIN tab_event t2
  ON t1.openid = t2.openid AND t1.cycle = t2.cycle AND t1.timestamp <= t2.timestamp
  AND event LIKE "club%"
)
, tab_channel_1 AS (
  SELECT DISTINCT
    date,
    openid,
    cycle,
    CASE
      WHEN event = "home_page_impression" THEN "home"
      WHEN event = "exploration_page_content_impression" THEN "探索-無腦刷"
      WHEN event = "club_post_page_view" AND JSON_EXTRACT_SCALAR(page_info, "$.source") IN ("notifications", "deeplink", "home", "exploration") THEN
        CASE
          WHEN JSON_EXTRACT_SCALAR(page_info, "$.source") IN ("notifications", "deeplink", "home") THEN JSON_EXTRACT_SCALAR(page_info, "$.source")
          ELSE 
            CASE
              WHEN lag_event = "exploration_page_item_click" THEN "探索-無腦刷"
              WHEN lag_event = "exploration_page_content_click" THEN IF(JSON_EXTRACT_SCALAR(lag_click_info, "$.sec") = "my_club", "探索-我的社團列", "探索-無腦刷")
              WHEN lag_event IN ("exploration_community_clubs_content_click", "exploration_community_page_view") THEN
                IF(JSON_EXTRACT_SCALAR(lag_page_info, "$.page") = "club_category", "所有社團頁", "頭像-我的社團列")
              ELSE "探索-我的社團列"
            END
        END
      ELSE
        CASE
          WHEN lag_event IN ("landing_page_view", "web_portal_page_view") THEN "deeplink"
          WHEN lag_event IN ("app_push_click", "app_push_notification_impression", "notification_content_impression") THEN "notifications"
          -- ELSE SPLIT(lag_event, "_")[SAFE_OFFSET(0)]
          ELSE SPLIT(JSON_EXTRACT_SCALAR(lag_page_info, "$.page"), "_")[SAFE_OFFSET(0)]
        END
    END AS channel
  FROM tab_first_club_event
  WHERE 
  (event IN ("home_page_impression", "exploration_page_content_impression"))
  OR
  (
    event = "club_post_page_view"
    AND JSON_EXTRACT_SCALAR(page_info, "$.source") IN ("notifications", "deeplink", "home", "exploration")
  )
  OR
  (
    event = "club_post_page_view"
    AND JSON_EXTRACT_SCALAR(page_info, "$.source") IN ("club_home", "club")
    AND lag_event NOT LIKE "club%"
  )
)
, tab_channel_2 AS (
  SELECT DISTINCT
    t1.date,
    t1.openid,
    t1.cycle,
    CASE
      WHEN t1.source_event = "exploration_page_content_click" THEN IF(JSON_EXTRACT_SCALAR(t1.source_click_info, "$.sec") = "my_club", "探索-我的社團列", "探索-無腦刷")
      WHEN t1.source_event IN ("exploration_community_clubs_content_click", "exploration_community_page_view") THEN
        IF(JSON_EXTRACT_SCALAR(t1.source_page_info, "$.page") = "club_category", "所有社團頁", "頭像-我的社團列")
      WHEN t1.source_event LIKE "exploration%" THEN "探索-我的社團列"
      WHEN t1.source_event LIKE "profile%" THEN "頭像-我的社團列"
      WHEN t1.source_event IN ("landing_page_view", "web_portal_page_view") THEN "deeplink"
      WHEN t1.source_event ="notification_content_click" THEN "notifications"
      WHEN t1.source_event LIKE "app_push%" THEN "notifications"
      WHEN t1.source_event LIKE "club%" THEN NULL
      WHEN t1.source_event ="sitewide_search" THEN "search"
      -- ELSE SPLIT(source_event, "_")[SAFE_OFFSET(0)]
      ELSE SPLIT(JSON_EXTRACT_SCALAR(t1.source_page_info, "$.page"), "_")[SAFE_OFFSET(0)]
    END AS channel
  FROM tab_club_source_event t1
)

  SELECT *
  FROM tab_channel_1
  UNION DISTINCT
  SELECT *
  FROM tab_channel_2
