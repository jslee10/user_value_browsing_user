CREATE OR REPLACE TABLE temp_data.js_user_value_contact_post_raw_event_2 
PARTITION BY date
AS

WITH tab_join_club AS (
  SELECT *
  FROM `analysis.js_ip_club_join_date`
)
, raw_event AS (
  SELECT 
    openid,
    timestamp,
    TIMESTAMP_MILLIS(CAST(timestamp AS INT64)) AS datetime,
    CAST(event_index AS FLOAT64) AS event_index,
    event,
    event_trigger,
    page_info,
    click_info,
    impression_info
  FROM `event.event_daily`
  WHERE property = "beanfun"
  AND date BETWEEN 20220101 AND 20240622
  AND openid IS NOT NULL
  AND openid IN (
    SELECT DISTINCT openid
    FROM tab_join_club
  )
  AND event NOT IN ("app_launch", "chatroom_page_gesture", "browser_item_click", "function_item_click", "app_theme")
)
, raw_event_with_lag_info AS (
  SELECT 
    *, 
    LAG(datetime) OVER (PARTITION BY openid ORDER BY timestamp, event_index) AS lag_datetime,
    LAG(event) OVER (PARTITION BY openid ORDER BY timestamp, event_index) AS lag_event,
    LAG(page_info) OVER (PARTITION BY openid ORDER BY timestamp, event_index) AS lag_page_info,
    LAG(click_info) OVER (PARTITION BY openid ORDER BY timestamp, event_index) AS lag_click_info,
  FROM raw_event
)
, raw_event_add_cycle AS (
  SELECT
    * EXCEPT (cycle_flg),
    SUM(cycle_flg) OVER (PARTITION BY openid ORDER BY timestamp, event_index) AS cycle 
  FROM (
    SELECT 
      *,
      CASE 
        WHEN event = "landing_page_view" AND JSON_EXTRACT_SCALAR(page_info, "$.page") = "splash" THEN 1
        WHEN DATETIME_DIFF(datetime, lag_datetime, MINUTE) > 10 THEN 1
        ELSE 0
      END AS cycle_flg
    FROM raw_event_with_lag_info
  )
)
, cycle_with_post_page_view AS (
  SELECT openid, cycle, club_id, MIN(timestamp) AS event_ts
  FROM raw_event_add_cycle
  INNER JOIN `server_data.beanfun_posts` t3
  ON JSON_EXTRACT_SCALAR(page_info, "$.uuid") = t3.id
  WHERE (
    event = "club_post_page_view"
    AND club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
    )
  )
  GROUP BY 1,2,3
)
, cycle_with_club_post_impression AS (
  SELECT openid, cycle, club_id, MIN(timestamp) AS event_ts
  FROM raw_event_add_cycle, UNNEST(JSON_EXTRACT_ARRAY(impression_info)) AS imp_item
  INNER JOIN `server_data.beanfun_posts` t3
  ON JSON_EXTRACT_SCALAR(imp_item, "$.uuid") = t3.id
  WHERE (
    event = "club_home_content_impression"
    AND JSON_EXTRACT_SCALAR(imp_item, "$.type") = "post"
    AND club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
    )
  )
  OR (
    event IN ("home_page_impression", "exploration_page_content_impression")
    AND JSON_EXTRACT_SCALAR(imp_item, "$.type") = "club_post"
    AND t3.club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
    )
  )
  GROUP BY 1,2,3
  HAVING COUNT(DISTINCT JSON_EXTRACT_SCALAR(imp_item, "$.uuid")) > 3
)
, cycle_merge AS (
  SELECT
    CASE
      WHEN t1.openid IS NOT NULL AND t2.openid IS NOT NULL THEN IF(t1.event_ts <= t2.event_ts, t1.openid, t2.openid)
      ELSE COALESCE(t1.openid, t2.openid) 
    END AS openid,
    CASE
      WHEN t1.cycle IS NOT NULL AND t2.cycle IS NOT NULL THEN IF(t1.event_ts <= t2.event_ts, t1.cycle, t2.cycle)
      ELSE COALESCE(t1.cycle, t2.cycle) 
    END AS cycle,
    CASE
      WHEN t1.club_id IS NOT NULL AND t2.club_id IS NOT NULL THEN IF(t1.event_ts <= t2.event_ts, t1.club_id, t2.club_id)
      ELSE COALESCE(t1.club_id, t2.club_id) 
    END AS club_id,
    CASE
      WHEN t1.event_ts IS NOT NULL AND t2.event_ts IS NOT NULL THEN IF(t1.event_ts <= t2.event_ts, t1.event_ts, t2.event_ts)
      ELSE COALESCE(t1.event_ts, t2.event_ts) 
    END AS event_ts,
    CASE
      WHEN t1.event_ts IS NOT NULL AND t2.event_ts IS NOT NULL THEN IF(t1.event_ts <= t2.event_ts, "post_pv", "post_imp")
      WHEN t1.event_ts IS NOT NULL THEN "post_pv"
      ELSE "post_imp"
    END AS contact_type
  FROM cycle_with_post_page_view t1
  FULL JOIN cycle_with_club_post_impression t2
  ON t1.openid = t2.openid AND t1.cycle = t2.cycle AND t1.club_id = t2.club_id
)
  SELECT *, DATE(datetime) AS date
  FROM raw_event_add_cycle t1
  INNER JOIN cycle_merge t2
  USING (openid, cycle)