
CREATE OR REPLACE TABLE temp_data.js_user_value_contact_post_contact_week AS

WITH df_cycle AS (
  WITH post_list AS (
    SELECT DISTINCT
      id,
      club_id,
      content,
      IF(REGEXP_CONTAINS(content, "指定任務|留言|獲得|抽獎|加票|得獎|中獎"), "campaign_post", "general_post") AS post_type
    FROM `server_data.beanfun_posts`
    WHERE club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
    )
  )
  , tab_contact_post_behavior AS (
    SELECT DISTINCT
      DATE(datetime) AS date,
      openid,
      cycle,
      club_id,
      event,
      CASE
        WHEN event = "club_post_page_view" THEN JSON_EXTRACT_SCALAR(page_info, "$.uuid") 
        ELSE JSON_EXTRACT_SCALAR(JSON_EXTRACT_ARRAY(impression_info)[SAFE_OFFSET(0)], "$.uuid")
      END AS post_id
    FROM `temp_data.js_user_value_contact_post_raw_event_2` t1
    INNER JOIN `analysis.js_user_value_ip_list` t2
    ON t1.club_id = t2.ip_id
    WHERE timestamp = event_ts
    AND event IN ("club_home_content_impression", "home_page_impression", "exploration_page_content_impression", "club_post_page_view")
  )

  SELECT DISTINCT t1.*, t2.content, t2.post_type
  FROM tab_contact_post_behavior t1
  INNER JOIN post_list t2
  ON t1.post_id = t2.id
)
, df_join AS (
  SELECT 
    t2.user_type, t1.openid, t1.club_id, t1.date AS join_date
  FROM `bf-data-normal-001.analysis.js_ip_club_join_date` t1
  INNER JOIN (
    SELECT DISTINCT 
      openid, club_id, club_name, type AS user_type
    FROM `bf-data-normal-001.analysis.user_value_score_browsing_user_1_join_club`
    -- WHERE type NOT IN ("優惠獵人", "湊熱鬧鄉民")
  ) t2
  ON t1.openid = t2.openid AND t1.club_id = t2.club_id
)
, df_merge AS (
  SELECT t2.user_type, t1.*, t2.join_date
  FROM df_cycle t1
  INNER JOIN df_join t2
  ON t1.openid = t2.openid AND t1.club_id = t2.club_id AND t1.date >= t2.join_date
)

  SELECT *, DENSE_RANK() OVER (PARTITION BY openid, club_id ORDER BY week_from_join) AS contact_order
  FROM (
    SELECT *, DATE_DIFF(date, join_date, WEEK) AS week_from_join
    FROM df_merge
    -- WHERE post_type = "general_post"
  )
  WHERE openid NOT IN (
  SELECT DISTINCT openid
  FROM `analysis.employee_tag`
  )
  