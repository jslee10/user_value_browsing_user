CREATE OR REPLACE TABLE `temp_data.js_user_value_deep_use_by_session` AS 

WITH tab_user_daily_first_timestamp AS (
  SELECT date, openid, CAST(first_usage_time AS FLOAT64) AS first_timestamp
  FROM `bf-data-normal-001.analysis.bf_active_user_info_utc0_dly`
)
, base AS (
  WITH raw_event AS (
    SELECT
      DATE(TIMESTAMP_MILLIS(CAST(timestamp AS INT64)), "Asia/Taipei") AS date,
      openid,
      CAST(timestamp AS FLOAT64) AS timestamp,
      CAST(event_index AS FLOAT64) AS event_index,
      event,
      page_info,
      click_info,
      impression_info
    FROM `event.event_daily`
    WHERE property = "beanfun"
    AND date BETWEEN 20220101 AND 20240622
    AND openid IS NOT NULL
    -- AND openid = "2090870847337222144"
  )
  SELECT
    t1.*,
    CAST(FLOOR((t1.timestamp - t2.first_timestamp)/1800000) AS INT64) AS session
  FROM raw_event t1
  INNER JOIN tab_user_daily_first_timestamp t2
  ON t1.openid = t2.openid AND t1.date = t2.date AND t1.timestamp >= t2.first_timestamp
)
, tab_click_post AS (
  SELECT
    date,
    session,
    t1.openid,
    JSON_EXTRACT_SCALAR(page_info, "$.uuid") AS post_id,
    t3.club_id,
    JSON_EXTRACT_SCALAR(page_info, "$.source") AS source
  FROM base t1
  INNER JOIN `server_data.beanfun_posts` t3
  ON JSON_EXTRACT_SCALAR(page_info, "$.uuid") = t3.id
  WHERE club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
    -- WHERE ip_type = "creator"
  )
  AND event = "club_post_page_view"
)
, tab_swap_club AS (
  SELECT
    date,
    session,
    t1.openid,
    t3.club_id,
    JSON_EXTRACT_SCALAR(imp_item, "$.uuid") AS post_id,
    timestamp
  FROM base t1, UNNEST(JSON_EXTRACT_ARRAY(impression_info)) AS imp_item
  INNER JOIN `server_data.beanfun_posts` t3
  ON JSON_EXTRACT_SCALAR(imp_item, "$.uuid") = t3.id
  WHERE event = "club_home_content_impression"
  AND JSON_EXTRACT_SCALAR(imp_item, "$.type") = "post"
  AND club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
    -- WHERE ip_type = "creator"
  )
)
, tab_swap_home AS (
  SELECT
    date,
    session,
    t1.openid,
    t3.club_id,
    JSON_EXTRACT_SCALAR(imp_item, "$.uuid") AS post_id,
    timestamp
  FROM base t1, UNNEST(JSON_EXTRACT_ARRAY(impression_info)) AS imp_item
  INNER JOIN `server_data.beanfun_posts` t3
  ON JSON_EXTRACT_SCALAR(imp_item, "$.uuid") = t3.id
  WHERE event = "home_page_impression"
  AND JSON_EXTRACT_SCALAR(imp_item, "$.type") = "club_post"
  AND club_id IN (
    SELECT DISTINCT ip_id
    FROM `analysis.js_user_value_ip_list`
    -- WHERE ip_type = "creator"
  )
)
, var_click_post AS (
  WITH tmp AS (
    SELECT
      date,
      session,
      openid,
      club_id AS ip_id,
      post_id,
      source,
      COUNT(*) AS cnt
    FROM tab_click_post
    GROUP BY 1,2,3,4,5,6
  )
  , tmp_json AS (
    SELECT
       date, session, openid, ip_id,
       TO_JSON(STRUCT(post_id AS post_id, source AS source, cnt AS cnt)) AS click_post
    FROM tmp
  )
  SELECT
    date, session, openid, ip_id, TO_JSON_STRING(ARRAY_AGG(click_post)) AS click_post
  FROM tmp_json
  GROUP BY 1,2,3,4
)
, var_swap_club AS (
  WITH tmp AS (
    SELECT
      date, session, openid, club_id AS ip_id, 
      ARRAY_AGG(DISTINCT post_id) AS post_id,
      (MAX(timestamp) - MIN(timestamp))/1000 AS ttl_time
    FROM tab_swap_club
    GROUP BY 1,2,3,4
  )
  SELECT
    date, session, openid, ip_id,
    TO_JSON_STRING(STRUCT(post_id AS post_id, ttl_time AS ttl_time)) AS swap_club
  FROM tmp
  WHERE ttl_time != 0 
)
, var_swap_home AS (
  WITH tmp AS (
    SELECT
      date, session, openid, club_id AS ip_id, 
      ARRAY_AGG(DISTINCT post_id) AS post_id,
      (MAX(timestamp) - MIN(timestamp))/1000 AS ttl_time
    FROM tab_swap_home
    GROUP BY 1,2,3,4
  )
  SELECT
    date, session, openid, ip_id,
    TO_JSON_STRING(STRUCT(post_id AS post_id, ttl_time AS ttl_time)) AS swap_home
  FROM tmp
  WHERE ttl_time != 0 
)
, var_merge AS (
  SELECT *
  FROM (
    SELECT *
    FROM var_click_post t1
    FULL JOIN var_swap_club t2
    USING (date, session, openid, ip_id)
    FULL JOIN var_swap_home t3
    USING (date, session, openid, ip_id)
  )
  WHERE date BETWEEN "2022-01-01" AND "2024-06-22"
  -- AND openid = "2090870847337222144"
)
, tab_join_club AS (
  WITH tmp AS (
    SELECT 
      DATE(TIMESTAMP_MILLIS(CAST(created_time AS INT64)), "Asia/Taipei") AS date,
      user_id,
      club_id
    FROM `server_data.beanfun_club_participants`
    WHERE club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
      -- WHERE ip_type = "creator"
    )
    AND role = "10"
    UNION DISTINCT
    SELECT 
      DATE(TIMESTAMP_MILLIS(CAST(join_time AS INT64)), "Asia/Taipei") AS date,
      user_id,
      club_id
    FROM `server_data.beanfun_club_participants_deleted`
    WHERE club_id IN (
      SELECT DISTINCT ip_id
      FROM `analysis.js_user_value_ip_list`
      -- WHERE ip_type = "creator"
    )
    AND role = "10"
  )
  SELECT
    t2.open_id AS openid,
    t1.club_id,
    MIN(t1.date) AS date
  FROM tmp t1
  INNER JOIN `server_data.beanfun_aliasid_openid_mapping` t2
  ON t1.user_id = t2.alias_id
  GROUP BY 1,2
)

SELECT
  CASE 
    WHEN t1.date > t2.date  THEN "after_join"
    WHEN t1.date = t2.date THEN "join"
    ELSE "before_join"
  END AS process,
  t1.date,
  t1.session,
  t1.openid,
  t1.ip_id,
  t3.ip_name,
  t1.* EXCEPT(date, session, openid, ip_id),
FROM var_merge t1
LEFT JOIN tab_join_club t2
ON t1.openid = t2.openid AND t1.ip_id = t2.club_id
INNER JOIN `analysis.js_user_value_ip_list` t3
ON t1.ip_id = t3.ip_id
ORDER BY openid, ip_id, date, session