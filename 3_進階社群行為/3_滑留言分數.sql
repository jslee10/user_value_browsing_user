
CREATE OR REPLACE TABLE temp_data.js_user_value_deep_behavior_cmt_gesture AS 

WITH post_pv AS (
  SELECT DISTINCT
    openid,
    DATE(datetime) AS date,
    JSON_EXTRACT_SCALAR(page_info, "$.uuid") AS post_id
  FROM `bf-data-normal-001.temp_data.js_user_value_contact_post_raw_event_2`
  WHERE event = "club_post_page_view"
  -- AND DATE(datetime) BETWEEN "2022-01-01" AND "2022-06-30"
)
, cmt_gesture AS (
  SELECT
    openid,
    DATE(datetime) AS date,
    JSON_EXTRACT_SCALAR(page_info, "$.uuid") AS post_id,
    COUNT(*) AS cnt_gesture
  FROM `bf-data-normal-001.temp_data.js_user_value_contact_post_raw_event_2`
  WHERE event = "club_post_comment_gesture"
  -- AND DATE(datetime) BETWEEN "2022-01-01" AND "2022-06-30"
  GROUP BY 1,2,3
)
, detect_gesture AS (
  SELECT 
    t1.*, 
    CASE 
      WHEN t2.cnt_gesture IS NULL or t2.cnt_gesture <= 2 THEN 0
      ELSE 1
    END AS cmt_gesture
  FROM post_pv t1
  LEFT JOIN cmt_gesture t2
  USING (openid, date, post_id)
)
, cmt_cnt_by_date AS (
  SELECT *, SUM(cnt_cmt) OVER (PARTITION BY date) AS cum_cnt_cmt
  FROM (
    SELECT  
      entity_id,
      PARSE_DATE("%Y%m%d", CAST(date AS STRING)) AS date,
      COUNT(DISTINCT id) AS cnt_cmt
    FROM `server_data.beanfun_comment`
    WHERE date >= 20220101
    GROUP BY 1,2
  )
  ORDER BY entity_id, date
)
, get_cmt_count AS (
  SELECT
    openid,
    t1.date,
    club_id,
    post_id,
    cmt_gesture,
    MAX(t3.cum_cnt_cmt) AS cnt_cmt
  FROM detect_gesture t1
  INNER JOIN `server_data.beanfun_posts` t2
  ON t1.post_id = t2.id
  LEFT JOIN cmt_cnt_by_date t3
  ON t1.post_id = t3.entity_id AND t1.date >= t3.date
  GROUP BY 1,2,3,4,5
  HAVING cnt_cmt >= 10
)
  SELECT openid, club_id, cnt_post_cmt_gesture, ctr_cmt_gesture
  FROM (
  SELECT DISTINCT
    openid,
    club_id,
    cmt_gesture,
    COUNT(DISTINCT post_id) OVER (PARTITION BY openid, club_id, cmt_gesture) AS cnt_post_cmt_gesture,
    COUNT(DISTINCT post_id) OVER (PARTITION BY openid, club_id, cmt_gesture)/COUNT(DISTINCT post_id) OVER (PARTITION BY openid, club_id) AS ctr_cmt_gesture
  FROM get_cmt_count
  )
  WHERE cmt_gesture = 1
  ORDER BY openid, club_id
  