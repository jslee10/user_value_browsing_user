CREATE OR REPLACE TABLE temp_data.js_user_value_deep_behavior_swap_post AS 

WITH tab_swap_user AS (
  SELECT 
    openid,
    ip_id,
    ip_name,
    date,
    session,
    ARRAY_LENGTH(JSON_EXTRACT_ARRAY(JSON_EXTRACT(swap_club, "$.post_id"))) AS cnt_swap,
    JSON_EXTRACT_SCALAR(swap_club, "$.ttl_time") AS ttl_time,
    ROW_NUMBER() OVER (PARTITION BY openid, ip_id ORDER BY date, session) AS order_club
  FROM `bf-data-normal-001.temp_data.js_user_value_deep_use_by_session` 
  WHERE process = "after_join"
  AND swap_club IS NOT NULL
) 
, tab_get_corr_score AS (
  SELECT openid, ip_id, IF(IS_NAN(corr_score), 0, corr_score) AS corr_score
  FROM (
  SELECT DISTINCT
    openid, ip_id,
    COUNT(*) OVER (PARTITION BY openid, ip_id) AS cnt_behavior,
    CORR(cnt_swap, order_club) OVER (PARTITION BY openid, ip_id) AS corr_score
  FROM tab_swap_user
  )
  WHERE cnt_behavior > 3
)

  SELECT
    t1.*,
    COALESCE(t2.corr_score, 0) AS corr_score
  FROM (
    SELECT
      openid,
      ip_id,
      COUNT(DISTINCT DATE_TRUNC(date, WEEK)) AS cnt_week
    FROM tab_swap_user
    GROUP BY 1,2
  ) t1
  LEFT JOIN tab_get_corr_score t2
  ON t1.openid = t2.openid AND t1.ip_id = t2.ip_id