CREATE OR REPLACE TABLE analysis.user_value_score_browsing_user_3_deep_use AS

SELECT *
FROM temp_data.js_user_value_deep_behavior_swap_post
FULL JOIN temp_data.js_user_value_deep_behavior_cmt_gesture
USING (openid, club_id)
